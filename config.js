// This is client side config only - don't put anything in here that shouldn't be public!
export const endpoint = `https://beer-fests-yoga.herokuapp.com/`
export const perPage = 4
export const pollIntervalInSeconds = 60
