import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import Title from './styles/Title'
import { BoothStyles, Country } from './styles/BoothStyles'
import Beer from './Beer'

class Booth extends Component {
  render() {
    const { booth } = this.props
    return (
      <BoothStyles>
        <Title>
          <Link
            href={{
              pathname: '/booth',
              query: { id: booth.id },
            }}
          >
            <a>{booth.name}</a>
          </Link>
        </Title>
        <Country>
          <p>{booth.country[0].brewery.country}</p>
        </Country>
        {booth.data.map(beer => (
          <Beer key={beer.id} beer={beer} />
        ))}
      </BoothStyles>
    )
  }
}

Booth.propTypes = {
  booth: PropTypes.object.isRequired,
}

export default Booth
