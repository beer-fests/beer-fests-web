import React, { Component } from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import Link from 'next/link'

import { pollIntervalInSeconds } from '../config'
import BeerDetails from './BeerDetails'
import Title from './styles/Title'
import { BoothStyles } from './styles/BoothStyles'

const SINGLE_BOOTH_QUERY = gql`
  query SINGLE_BOOTH_QUERY($id: ID!) {
    booth(where: { id: $id }) {
      id
      name
      beers {
        id
        name
        type
        precentage
        onTap
        special
      }
    }
  }
`

class BoothDetails extends Component {
  state = {}

  handleChange = e => {
    const { name, type, value } = e.target
    let val
    switch (type) {
      case 'number':
        val = parseFloat(value)
        break
      case 'checkbox':
        val = e.target.checked
        break
      default:
        val = value
        break
    }
    this.setState({ [name]: val })
  }
  updateBeer = async (e, updateBeerMutation) => {
    e.preventDefault()
    const res = await updateBeerMutation({
      variables: {
        id: this.props.id,
        ...this.state,
      },
    })
  }
  render() {
    return (
      <Query
        query={SINGLE_BOOTH_QUERY}
        pollInterval={pollIntervalInSeconds * 1000}
        variables={{
          id: this.props.id,
        }}
      >
        {({ data, error, loading }) => {
          if (loading) return <p>Loading..</p>
          if (error) return <p>Error: {error.message}</p>
          if (!data.booth) return <p>No Booth found for {this.props.id}</p>
          const booth = data.booth
          return (
            <BoothStyles>
              <Title>
                <Link
                  href={{
                    pathname: '/booth',
                    query: { id: booth.id },
                  }}
                >
                  <a>{booth.name}</a>
                </Link>
              </Title>
              {booth.beers.map(beer => (
                <BeerDetails id={beer.id} />
              ))}
            </BoothStyles>
          )
        }}
      </Query>
    )
  }
}

export default BoothDetails
