import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import BeerStyles from './styles/BeerStyles'

class Beer extends Component {
  render() {
    const { beer } = this.props
    return (
      <BeerStyles>
        <p className={beer.onTap ? 'onTap' : ''}>
          <div className="description">
            <Link
              href={{
                pathname: '/beer',
                query: { id: beer.id },
              }}
            >
              <a>{beer.name}</a>
            </Link>
            <div className="type">{beer.type}</div>
          </div>
          <div className="precentage">{beer.precentage}</div>
        </p>
      </BeerStyles>
    )
  }
}

Beer.propTypes = {
  beer: PropTypes.object.isRequired,
}

export default Beer
