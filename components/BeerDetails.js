import React, { Component } from 'react'
import { Mutation, Query } from 'react-apollo'
import gql from 'graphql-tag'
import styled from 'styled-components'

import Toggle from 'react-toggle'
import ToggleStyles from './styles/ToggleStyles'
import Form from './styles/Form'
import { pollIntervalInSeconds } from '../config'

const SINGLE_BEER_QUERY = gql`
  query SINGLE_BEER_QUERY($id: ID!) {
    beer(where: { id: $id }) {
      id
      name
      type
      precentage
      onTap
      special
    }
  }
`

const UPDATE_BEER_MUTATION = gql`
  mutation UPDATE_BEER_MUTATION(
    $id: ID!
    $name: String
    $type: String
    $precentage: String
    $onTap: Boolean
    $special: Boolean
  ) {
    updateBeer(
      id: $id
      name: $name
      type: $type
      precentage: $precentage
      onTap: $onTap
      special: $special
    ) {
      id
      name
      type
      precentage
      onTap
      special
    }
  }
`

const Center = styled.div`
  text-align: center;
`

class BeerDetails extends Component {
  state = {}

  handleChange = e => {
    const { name, type, value } = e.target
    let val
    switch (type) {
      case 'number':
        val = parseFloat(value)
        break
      case 'checkbox':
        val = e.target.checked
        break
      default:
        val = value
        break
    }
    console.log(name, val)
    this.setState({ [name]: val })
    console.log(this.state)
  }
  updateBeer = async (e, updateBeerMutation) => {
    e.preventDefault()
    const res = await updateBeerMutation({
      variables: {
        id: this.props.id,
        ...this.state,
      },
    })
  }
  render() {
    return (
      <Query
        query={SINGLE_BEER_QUERY}
        pollInterval={pollIntervalInSeconds * 1000}
        variables={{
          id: this.props.id,
        }}
        onCompleted={this.setBeer}
      >
        {({ data, error, loading }) => {
          if (loading) return <p>Loading..</p>
          if (error) return <p>Error: {error.message}</p>
          if (!data.beer) return <p>No Beer found for {this.props.id}</p>
          const beer = data.beer
          return (
            <Mutation mutation={UPDATE_BEER_MUTATION} variables={this.state}>
              {(updateBeer, { loading, error }) => (
                <Form onSubmit={e => this.updateBeer(e, updateBeer)}>
                  <fieldset disabled={loading} aria-busy={loading}>
                    <label htmlFor="name">
                      Name
                      <input
                        type="text"
                        id="name"
                        name="name"
                        placeholder="Name"
                        required
                        defaultValue={beer.name}
                        onChange={this.handleChange}
                      />
                    </label>
                    <label htmlFor="type">
                      Type
                      <input
                        type="text"
                        id="type"
                        name="type"
                        placeholder="Type"
                        required
                        defaultValue={beer.type}
                        onChange={this.handleChange}
                      />
                    </label>
                    <label htmlFor="precentage">
                      Precentage
                      <input
                        type="text"
                        id="precentage"
                        name="precentage"
                        placeholder="Precentage"
                        required
                        defaultValue={beer.precentage}
                        onChange={this.handleChange}
                      />
                    </label>
                    <label htmlFor="onTap">
                      <ToggleStyles>
                        <Toggle
                          defaultChecked={beer.onTap}
                          id="onTap"
                          name="onTap"
                          ref="onTap"
                          onChange={this.handleChange}
                        />
                        <span>onTap?</span>
                      </ToggleStyles>
                    </label>
                    <label htmlFor="special">
                      <ToggleStyles>
                        <Toggle
                          defaultChecked={beer.special}
                          id="special"
                          name="special"
                          onChange={this.handleChange}
                        />
                        <span>Special</span>
                      </ToggleStyles>
                    </label>
                    <button type="submit">
                      Sav{loading ? 'ing' : 'e'} Changes
                    </button>
                  </fieldset>
                </Form>
              )}
            </Mutation>
          )
        }}
      </Query>
    )
  }
}

export default BeerDetails
