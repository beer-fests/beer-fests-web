import React, { Component } from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import styled from 'styled-components'

import Booth from './Booth'

import { pollIntervalInSeconds } from '../config'

const GET_BOOTHS = gql`
  query GET_BOOTHS {
    booths {
      id
      name
      country: beers(first: 1) {
        brewery {
          country
        }
      }
      data: beers {
        id
        name
        type
        precentage
        onTap
        special
        brewery {
          id
          name
        }
      }
    }
  }
`

const GET_BOOTHS_ON_TAP = gql`
  query GET_BOOTHS_ON_TAP {
    booths {
      id
      name
      country: beers(first: 1) {
        brewery {
          country
        }
      }
      data: beers(where: { onTap: true }) {
        id
        name
        type
        precentage
        onTap
        special
        brewery {
          id
          name
        }
      }
    }
  }
`

const Center = styled.div`
  text-align: center;
`

const BoothsList = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 30px;
  max-width: ${props => props.theme.maxWidth};
  margin: 0 auto;
`

class BeerOverview extends Component {
  render() {
    return (
      <Query
        query={this.props.onTap ? GET_BOOTHS_ON_TAP : GET_BOOTHS}
        pollInterval={pollIntervalInSeconds * 1000}
      >
        {({ data, error, loading }) => {
          if (loading) return <p>Loading..</p>
          if (error) return <p>Error: {error.message}</p>
          return (
            <BoothsList>
              {data.booths.map(booth => (
                <Booth key={booth.id} booth={booth} />
              ))}
            </BoothsList>
          )
        }}
      </Query>
    )
  }
}

export default BeerOverview
