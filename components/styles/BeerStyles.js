import styled from 'styled-components'

const BeerStyles = styled.div`
  p {
    font-size: 12px;
    height: 5rem;
    line-height: 2;
    flex-grow: 1;
    padding: 0 3rem;
    background: ${props => props.theme.orange};
    font-size: 1.5rem;
    display: flex;
    flex-direction: 'row';
  }

  .onTap {
    background: rgba(218, 119, 19, 1);
  }

  .description {
    width: 90%;
  }
  .precentage {
    width: 10%;
  }
  .type {
    padding-left: 10px;
    font-size: 0.9rem;
    color: rgba(66, 66, 66, 1);
  }
`

export default BeerStyles
