import Link from 'next/link'
import NavStyles from './styles/NavStyles'

const Nav = () => (
  <NavStyles>
    <Link href="/beerOverview">
      <a>Bier en Big</a>
    </Link>
    <Link href="/onTap">
      <a>onTap</a>
    </Link>
  </NavStyles>
)

export default Nav
