import BoothDetails from '../components/BoothDetails'

const Booth = props => (
  <div>
    <BoothDetails id={props.query.id} />
  </div>
)

export default Booth
