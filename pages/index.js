import BeerOverview from '../components/BeerOverview'

const Home = props => (
  <div>
    <BeerOverview />
  </div>
)

export default Home
