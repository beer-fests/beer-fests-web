import BeerDetails from '../components/BeerDetails'

const Beer = props => (
  <div>
    <BeerDetails id={props.query.id} />
  </div>
)

export default Beer
