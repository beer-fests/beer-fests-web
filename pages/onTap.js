import BeerOverview from '../components/BeerOverview'

const OnTap = props => (
  <div>
    <BeerOverview onTap={true} />
  </div>
)

export default OnTap
