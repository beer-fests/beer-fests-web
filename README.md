# Beer-fests-web 

## Purpose

Beer-fests web frontend to make development less painful.

## Usage

### Install dependencies
```
yarn
```

### Start web client
Make sure you run the [beer-fests-api](https://gitlab.com/beer-fests/beer-fests-api) on your local machine on port 4444.

After that you can start developing the web client frontend.

```
yarn dev
```

Use your ip-address to browse to the site. f.e. http://10.31.40.66:7777

### Credits

Most of this setup is stolen from Wes Bos amazing course Advanced React.